#!/bin/bash

rm -rf pybufrkit
wget -O pbk.zip https://github.com/ywangd/pybufrkit/archive/master.zip
unzip pbk.zip
mv pybufrkit-master/pybufrkit .
rm -rf pbk.zip pybufrkit-master

pip download bitstring==3.1.5
unzip bitstring-3.1.5.zip
mv -f bitstring-3.1.5/bitstring.py .
rm -rf bitstring-3.1.5.zip bitstring-3.1.5
