from __future__ import print_function

import json

import boto3

from pybufrkit.decoder import Decoder
from pybufrkit.renderer import FlatTextRenderer, NestedTextRenderer, FlatJsonRenderer, NestedJsonRenderer


def read_s3_object(object_key):
    s3 = boto3.resource('s3')
    bucket_name = 'aws-bufr-upload'
    print('Reading {}/{}'.format(bucket_name, object_key))
    bucket = s3.Bucket(bucket_name)
    obj = bucket.Object(object_key)
    return obj.get()['Body'].read()


def save_s3_objet(object_key, s):
    s3 = boto3.resource('s3')
    bucket_name = 'aws-bufr-download'
    print('Saving {}/{}'.format(bucket_name, object_key))
    bucket = s3.Bucket(bucket_name)
    if not isinstance(s, str):
        s = json.dumps(s, encoding='latin-1', indent=2)
    bucket.put_object(Key=object_key, ACL='public-read', Body=s)


def error_response(code, description):
    return {
        'statusCode': code,
        'headers': {  # enable CORS
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
        },
        'body': json.dumps({
            'ok': False,
            'description': description
        })
    }


def process(event, context):
    print(json.dumps(event))

    try:
        object_key = event['queryStringParameters']['object']
    except (TypeError, KeyError):
        return error_response(400, 'Object key not provided')

    try:
        output_format = event['queryStringParameters']['outputFormat']
    except (TypeError, KeyError):
        output_format = 'nestedText'

    return decode_bucket_object(object_key, output_format)


def decode_bucket_object(object_key, output_format):
    print('Decoding ...')

    renderers = {
        'flatText': FlatTextRenderer(),
        'nestedText': NestedTextRenderer(),
        'flatJSON': FlatJsonRenderer(),
        'nestedJSON': NestedJsonRenderer(),
    }

    try:
        s = read_s3_object(object_key)
    except Exception as e:
        print(e)
        return error_response(400, 'Error reading {}'.format(object_key))

    decoder = Decoder()
    renderer = renderers.get(output_format, renderers['nestedText'])
    try:
        bufr_message = decoder.process(s, file_path=object_key)
    except Exception as e:
        return error_response(400, 'Error when decoding: {}'.format(e))

    try:
        output_key = '{}.txt'.format(object_key)
        save_s3_objet(output_key, renderer.render(bufr_message))
    except Exception as e:
        return error_response(400, 'Error saving decoded result: {}'.format(e))

    response = {
        'statusCode': 200,
        'headers': {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": True,
        },
        'body': json.dumps({
            'object': output_key
        })
    }

    return response
